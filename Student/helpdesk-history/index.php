<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
$user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Student/helpdesk-history/helpdeskBL-history.php');
?>

<html>
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Helpdesk History</title>
    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
		<link rel="stylesheet" href="/assets/css/styles.css">
  </head>
	
	<body>
		<div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
							<li class="active" role="presentation"><a href="#">Help Desk</a></li>
                    <li role="presentation"><a href="/Student/transportation/">Transportation </a></li>
                    <li role="presentation"><a href="/Student/changepassword/">Change Password</a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label>
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
			
			
			<div id="sub">
        <div class="ulLink">
            <ul>
              <li><a href="/Student/helpdesk/">Form</a></li>
              <li>|</li>
              <li class="active">History of Submission</li>       
            </ul>
          </div>
     </div>
			<div id="form">
        <div id="hi">
          <h3>Helpdesk History</h3>
          <p>List of your helpdesk form &nbsp; </p>
        </div>
      </div>
			
			<div id="parcelform">
        <table id="userz" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
              <th>Title</th>
              <th>Description</th>
              <th>Name</th>
              <th>Student ID</th>
              <th>Date</th>
              <th>Status</th>
							<th>Remark</th>
						</tr>
					</thead>
					
					<tbody>
						<?php
              if(mysqli_num_rows($result) > 0){
                while($row=mysqli_fetch_array($result)){
                  ?>
                  <tr>
                    <td><?php echo $row['hd_ID'];?></td>
                    <td><?php echo $row['hd_title'];?></td>
                    <td><?php echo $row['hd_description'];?></td>
                    <td><?php echo $row['ct_name'];?></td>
                    <td><?php echo $row['hd_us_ID_fk'];?></td>
                    <td><?php echo $row['hd_date'];?></td>
										<td><?php echo $row['hd_status'];?></td>
										<td><?php echo $row['hd_remarks'];?></td>
									</tr>
                  <?php
                }
              }
              ?>
					</tbody>
				</table>				
		</div>
	</body>
</html>

	<script src="/assets/js/jquery.min.js"></script>
      <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
      <script src="/assets/js/jquery.dataTables.min.js"></script>
      <script src="/assets/js/dataTables.bootstrap.min.js"></script>
      <script src="/assets/js/bootstrap-dialog.min.js"></script>
      <script type="text/javascript">
				
			$(document).ready(function() {
    		$('#userz').DataTable( {
        	"order": [ 0, "desc" ]
				})
			} );	

			</script>