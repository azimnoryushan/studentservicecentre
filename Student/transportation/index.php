<!DOCTYPE html>
<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
$t_user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Student/transportation/transportationBL.php');

?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Transportation Page</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    
    <link href="/assets/css/datepicker.min.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li role="presentation"><a href="/Student/helpdesk/">Help Desk</a></li>
              <li class="active" role="presentation"><a href="#">Transportation </a></li>
              <li role="presentation"><a href="/Student/changepassword/">Change Password</a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
     <div id="sub">
        <div class="ulLink">
            <ul>
              <li class="active">Form</li>
              <li>|</li>
              <li><a href="/Student/transportation-history/">History of Submission</a></li>       
            </ul>
          </div>
     </div>
     <div id="formTH">
       
     
      <div id="hi">
        <h3>Transportation Booking</h3>
        <p>Fill up this form to request transportations, request must be made at least a week before the actual event.</p>
      </div>

      <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" role="form" class="form-horizontal">
        <div class="form-group">
          <label class="control-label">Purpose <span style="color: #FF0000;">*</span></label>
          <input type="text" class="form-control" name="purpose" placeholder="Enter the purpose of the trip" onkeypress='return event.charCode == 8 || event.charCode == 32 || event.charCode == 44 || event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57) || (event.charCode >=65 && event.charCode <= 90) || (event.charCode >=97 && event.charCode <= 122)' maxlength="70" required/></div>
        <div class="form-group">
          <label class="control-label">Anticipated Head Count <span style="color: #FF0000;">*</span></label>
          <input class="form-control" id="radioheadcount" type="number" name="headcount" placeholder="1" min="1" max="120" style="width:200px;" required>
        </div>

        <div class="form-group" style="width:336px;min-width:0px;">
          <label class="control-label">Type of transportation required <span style="color: #FF0000;">*</span></label>
          <div class="radio">
            <label class="control-label">
                        <input type="radio" id="radiobus" name="typetrans" value="<?php $_POST['typetrans']="Bus"; echo $_POST['typetrans']; ?>" checked>Bus</label><br>
            <label class="control-label">
                        <input type="radio" id="radiovan" name="typetrans" value="<?php $_POST['typetrans']="Van"; echo $_POST['typetrans']; ?>">Van</label>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label">Organizing Club Name <span style="color: #FF0000;">*</span></label>
          <input class="form-control" type="text" placeholder="SRC/KPU/Peers/Rakan Usrah/Others..." name="clubname" onkeypress='return event.charCode == 8 || event.charCode == 32 || event.charCode == 44 || event.charCode >= 48 && event.charCode <= 57 || event.charCode >=65 && event.charCode <= 90 || event.charCode >=97 && event.charCode <= 122' maxlength="50" required></div>

        <div class="form-group">
          <label class="control-label">Destination <span style="color: #FF0000;">*</span></label>
          <input class="form-control" type="text" placeholder="Enter the destination going" name="destination" onkeypress='return event.charCode == 8 || event.charCode == 32 || event.charCode == 44 || event.charCode == 46 || event.charCode >= 48 && event.charCode <= 57 || event.charCode >=65 && event.charCode <= 90 || event.charCode >=97 && event.charCode <= 122' maxlength="100" required></div>

        <div class="form-group">
          <label class="control-label">Accompanying Staff <span style="color: #FF0000;">*</span></label>
          <input class="form-control" type="text" placeholder="Enter the staff name" name="accstaff" onkeypress='return event.charCode == 8 || event.charCode == 32 || event.charCode == 44 || event.charCode == 46 || event.charCode >=65 && event.charCode <= 90 || event.charCode >=97 && event.charCode <= 122' maxlength="100" required></div>

        <div class="form-group">
          <label class="control-label">Contact Number <span style="color: #FF0000;">*</span></label>
          <input class="form-control" type="text" placeholder="60121234540" name="contactnum" onkeypress='return event.charCode == 8 || event.charCode >= 48 && event.charCode <= 57' maxlength="20" required></div>

        <div class="form-group">
          <label class="control-label">Date & Time From <span style="color: #FF0000;">*</span></label>
          <input class="form-control datepicker-here minMaxExample" data-language='en' type='text' data-timepicker="true" name="datetimefrom" placeholder="yyyy/mm/dd --:--:--" required></div>

        <div class="form-group">
          <label class="control-label">Date & Time To <span style="color: #FF0000;">*</span></label>
          <input class="form-control datepicker-here minMaxExample" type='text' data-language='en' data-timepicker="true" name="datetimeto" placeholder="yyyy/mm/dd --:--:--" required></div>

        <input class="btn btn-default" type="submit" value="Submit" name="submit">
      </form>

    </div>
    
  </body>

  </html>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-animation.js"></script>
    <script src="/assets/js/datepicker.min.js"></script>
    <script src="/assets/js/datepicker.en.js"></script>
    <script src="/assets/js/script.js"></script>
    <script type="text/javascript">
    $('#radioheadcount').keyup(function(){
      if(parseInt($(this).val()) > 12) $('#radiovan').attr('disabled', '');
      else $('#radiovan').removeAttr('disabled');
    })
</script>
     