<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
$user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Student/transportation-history/transportationBL-history.php');
?>

<html>
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Transportation History</title>
    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
		<link rel="stylesheet" href="/assets/css/styles.css">
  </head>
	
	<body>
		<div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li role="presentation"><a href="/Student/helpdesk/">Help Desk</a></li>
              <li  class="active"role="presentation"><a href="#">Transportation </a></li>
              <li role="presentation"><a href="/Student/changepassword/">Change Password</a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label>
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
			
			
			<div id="sub">
        <div class="ulLink">
            <ul>
              <li><a href="/Student/transportation/">Form</a></li>
              <li>|</li>
              <li class="active">History of Submission</li>       
            </ul>
          </div>
     </div>
			<div id="form">
        <div id="hi">
          <h3>Transportation</h3>
          <p>Admin can approve or reject the transportation request &nbsp; </p>
        </div>
      </div>
			
			<div id="parcelform">
        <table id="userz" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
              <th>From (DateTime)</th>
              <th>To (DateTime)</th>
              <th>Purpose</th>
              <th>Club Name</th>
              <th>Destination</th>
              <th>Transport Type</th>
              <th>Status</th>
              <th>Remark</th>
						</tr>
					</thead>
					
					<tbody data-link="row" class="rowlink">
						<?php
              if(mysqli_num_rows($result) > 0){
                while($row=mysqli_fetch_array($result)){
                  ?>
                  <tr>
                    <td><?php echo $row['tb_ID'];?></td>								
										<td><?php echo $row['tb_datetimeFrom'];?></td>
										<td><?php echo $row['tb_datetimeTo'];?></td>
										<td><?php echo $row['tb_purpose'];?></td>
										<td><?php echo $row['tb_clubName'];?></td>
										<td><?php echo $row['tb_destination'];?></td>
										<td><?php echo $row['tb_type'];?></td>
										<td><?php echo $row['tb_status'];?></td>
										<td><?php echo $row['tb_remarks'];?></td>
									</tr>
                  <?php
                }
              }
              ?>
					</tbody>
				</table>				
		</div>
	</body>
</html>

	<script src="/assets/js/jquery.min.js"></script>
      <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
      <script src="/assets/js/jquery.dataTables.min.js"></script>
      <script src="/assets/js/dataTables.bootstrap.min.js"></script>
      <script src="/assets/js/bootstrap-dialog.min.js"></script>
      <script type="text/javascript">
				
			$(document).ready(function() {
    		$('#userz').DataTable( {
        	"order": [ 0, "desc" ]
				})
			} );

</script>