<!DOCTYPE html>
<?php
session_start();

include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Student/helpdesk/helpdeskBL.php');
?>


<html>

<header>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Help Desk Page</title>
  <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/Contact-Form-Clean.css">
  <link rel="stylesheet" href="/assets/css/Footer-Basic.css">
  <link rel="stylesheet" href="/assets/css/Navigation-Clean1.css">
  <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
  <link rel="stylesheet" href="/assets/css/styles.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
</header>

<body>
  <div>
    <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav">
                    <li class="active" role="presentation"><a href="#">Help Desk</a></li>
                    <li role="presentation"><a href="/Student/transportation/">Transportation </a></li>
                    <li role="presentation"><a href="/Student/changepassword/">Change Password</a></li>
                </ul>
                <p class="navbar-text navbar-right actions">
                  <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label>  
                  <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
            </div>
        </div>
    </nav>
  </div>
    <div id="sub">
        <div class="ulLink">
            <ul>
              <li class="active">Form</li>
              <li>|</li>
              <li><a href="/Student/helpdesk-history/">History of Submission</a></li>       
            </ul>
          </div>
     </div>
    <div id="form">
         
        <div id="hi">
            <h3>Help Desk</h3>
            <p>This a platform for you to give feedback, complaint and suggestion for UniMy. &nbsp; </p>
        </div>
    <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF'];?>" method ="POST" role="form" class ="form-horizontal">

      <div class="form-group">
        <label class="control-label">Title <span style="color: #FF0000;">*</span> </label>
        <input type="text" class="form-control" name="hd_title" onkeypress='return event.charCode = 32 || event.charCode == 44 || event.charCode == 46 || event.charCode >= 48 && event.charCode <= 57 || event.charCode >=65 && event.charCode <= 90 || event.charCode >=97 && event.charCode <= 122' maxlength="70" required />
      </div>

      <div class="form-group">
        <label class="control-label">Type of form <span style="color: #FF0000;">*</span> </label>
     		<select class="form-control" name="ct_ID" required>
          <option value=""></option>
     			<option value="<?php $_POST['ct_ID']="1"; echo $_POST['ct_ID'];?>">Feedback</option>
     			<option value="<?php $_POST['ct_ID']="2"; echo $_POST['ct_ID'];?>">Suggestion</option>
     			<option value="<?php $_POST['ct_ID']="3"; echo $_POST['ct_ID'];?>">Complaint</option>
     		</select>
     	</div>

      <div class="form-group">
        <label class="control-label">Department <span style="color: #FF0000;">*</span> </label>
        <select class="form-control" name="sub_ID" required>
          <option value=""></option>
     			<option value="<?php $_POST['sub_ID']="ST0001"; echo $_POST['sub_ID'];?>">Head of Centre Student Experience</option>
     			<option value="<?php $_POST['sub_ID']="ST0002"; echo $_POST['sub_ID'];?>">Head of Finance</option>
          <option value="<?php $_POST['sub_ID']="ST0003"; echo $_POST['sub_ID'];?>">Head of Academic (Bachelor)</option>
          <option value="<?php $_POST['sub_ID']="ST0004"; echo $_POST['sub_ID'];?>">Head of Academic (Diploma)</option>
          <option value="<?php $_POST['sub_ID']="ST0005"; echo $_POST['sub_ID'];?>">Head of Academic (Foundation)</option>
          <option value="<?php $_POST['sub_ID']="ST0006"; echo $_POST['sub_ID'];?>">Head of Security</option>
          <option value="<?php $_POST['sub_ID']="ST0007"; echo $_POST['sub_ID'];?>">Head of IT Department</option>
          <option value="<?php $_POST['sub_ID']="ST0008"; echo $_POST['sub_ID'];?>">Head of Examination Unit</option>
     			<option value="<?php $_POST['sub_ID']="ST0009"; echo $_POST['sub_ID'];?>">Librarian</option>
     		</select>
      </div>

      <div class="form-group">
        <label class="control-label">Description <span style="color: #FF0000;">*</span> </label>
        <textarea class="form-control" rows="14" placeholder="Tell us what's your thoughts" name="hd_description" maxlength="700" required></textarea>
      </div>
      <!--Submit-->
      <input class="btn btn-default" type="submit" value="Submit" name= "submit">
    </form>
  </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-animation.js"></script>

</body>

</html>
