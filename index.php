<?php
include($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
session_start();
$_SESSION['myuserID'] = '';
// if(isset($_SESSION['myuserID'])){
//   $_SESSION['myuserID'] = $role;
//       if($role == 'ADMIN'){
//           header("Location:Admin/parcel/");
          
//         }else if($role == 'STUD'){
//           header("Location:Student/helpdesk/");
          
//         }
//         else if($role == 'SUPERADMIN'){
//           header("Location:Super-Admin/adduser/");
//         }
//         else if($role == 'HOD'){
//           header("Location:HOD/list/");
//         }

// }

if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form
      $userID =$_REQUEST['myuserID'];
      $password =$_REQUEST['mypassword'];
      $password =md5($password);
      $sql = "SELECT us_ID, us_password ,us_role FROM user WHERE us_ID = '$userID' AND us_password ='$password'";
      $result = mysqli_query($conn,$sql);
      $userinfo = mysqli_fetch_array($result);

      $rows = mysqli_num_rows($result);
      $role= $userinfo['us_role'];

      if(isset($rows)==1){
        //$IP = getenv ("REMOTE_ADDR");
        $_SESSION['myuserID']=$userinfo['us_ID'];
        //$_SESSION['clientip'] = $IP;

        $_SESSION['role'] = $role;
        if($role == 'ADMIN'){
          header("Location:Admin/parcel/");
          
        }else if($role == 'STUD'){
          header("Location:Student/helpdesk/");
          
        }
        else if($role == 'SUPERADMIN'){
          header("Location:Super-Admin/adduser/");
        }
        else if($role == 'HOD'){
          header("Location:HOD/list/");
        }

      }
      else{
        ?>
        <script>
           alert("Invalid User ID or Password.");
        </script>
        <?php 
      }
     }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="login-clean">
        <form action="index.php"method="post">
            <h3 class="text-center" style="margin:0px 0px 20px;">Login</h3>
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><img class="img-thumbnail img-responsive" src="assets/img/cse logo.jpg"></div>
            <div class="form-group">
                <input class="form-control" type="text" name="myuserID" placeholder="ID" onkeypress='return event.charCode == 8 || event.charCode == 32 || event.charCode >= 48 && event.charCode <= 57 || event.charCode >=65 && event.charCode <= 90 || event.charCode >=97 && event.charCode <= 122'required>
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="mypassword" placeholder="Password" onkeypress='return event.charCode == 8 || event.charCode == 32 || event.charCode >= 48 && event.charCode <= 57 || event.charCode >=65 && event.charCode <= 90 || event.charCode >=97 && event.charCode <= 122'required>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit" id="loginname" name="login" style="background-color:rgb(86,198,198);">Log In</button>
            </div>
        </form>
    </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script>
    $(document).keypress(function (e) {
    if (e.which == 13) {
        $('form#loginname').trigger();
        return false;
    }
});
  </script>
</body>

</html>
