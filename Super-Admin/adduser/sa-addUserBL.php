<?php
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['SUBMIT'])){
  include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
	$userID = $_SESSION['myuserID'];
	
	$sa_id = strtoupper(mysqli_real_escape_string($conn, $_POST['id'])); //done uppercase, name
	$sa_name = strtoupper(mysqli_real_escape_string($conn, $_POST['name']));
	$sa_email = mysqli_real_escape_string($conn, $_POST['email']);
	$sa_contact = mysqli_real_escape_string($conn, $_POST['contact']);
	$sa_nric = mysqli_real_escape_string($conn, $_POST['nric']);
	$sa_role = substr($sa_id, 0, 1);
	
	
	if ($sa_role == "B" || $sa_role == "F" || $sa_role == "D"){
    $sa_role = 'STUD';
		$sa_password = md5($sa_nric); //default password STUD = IC no.
  }else if ($sa_role == "P"){
    $sa_role = 'ADMIN';
		$sa_password = md5('1234'); //default password ADMIN = 1234
  }else if ($sa_role == "H"){
		$sa_role = 'HOD';
		$sa_password = md5('1234'); //default password ADMIN = 1234
	}else if ($sa_role == "R"){
		$sa_role = 'SUBSCRIBER';
		$sa_password = md5('1234'); //default password ADMIN = 1234
	}else{
		?>
		<script> alert("Invalid role");</script>
		<?php
	}
	
	//audit activity
  $au_activity ="ADDED user ID: $sa_id Name: $sa_name";
	
	$sql_insert_by_one = "INSERT INTO user (us_ID, us_name, us_password, us_email, us_contactNo, us_role, us_NRIC)
  VALUES ('$sa_id', '$sa_name', '$sa_password', '$sa_email', '$sa_contact', '$sa_role', '$sa_nric')";
	
	$au_sql = "INSERT INTO audit (au_user_ID, au_timeLog, au_activity) VALUES ('$userID', now(), '$au_activity')";

	if(mysqli_query($conn, $sql_insert_by_one) && mysqli_query($conn,$au_sql)){
    ?>
      <script>alert("Succesfully added.");</script>
    <?php
  }else{
    ?>
      <script>alert("Fail to add");</script>
    <?php
  }
	
}
// --------- INSERT BY-FILE USER ---------------------------------------------------------------
else if(isset($_POST["IMPORT"])){
	// --------- DEFINE FILE NAME ----------------------------------------------------------------
  $filename = $_FILES["file"]["tmp_name"];

  if($_FILES["file"]["size"] > 0){
    $file = fopen($filename, "r");

    while(($getData = fgetcsv($file, 10000, ",")) !== FALSE){

      $sql = "INSERT INTO user (us_ID, us_name, us_password, us_email, us_contactNo, us_role, us_NRIC)
      VALUES ('".$getData[0]."','".$getData[1]."',MD5('".$getData[2]."'),'".$getData[3]."','".$getData[4]."','".$getData[5]."','".$getData[6]."')";

      $result = mysqli_query($conn, $sql);
      
    }
    fclose($file);
		if(!isset($result)){
        ?>
					<script>alert("Failed to import");</script>
				<?php
      }else{
        ?>
					<script>alert("Succesfully imported");</script>
				<?php
      }
  }
}
?>