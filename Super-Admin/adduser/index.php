<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Super-Admin/adduser/sa-addUserBL.php');
?>

<html>
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add User - Super Admin</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link href="/assets/css/datepicker.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
  </head>
	
	<body>
		<div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li class="active" role="presentation"><a href="#">Add User </a></li>
							<li role="presentation"><a href="/Super-Admin/deleteuser/">Update | Delete User</a></li>
							<li role="presentation"><a href="/Super-Admin/auditlog/">Audit Log</a></li>
							<li role="presentation"><a href="/Super-Admin/changepassword/">Change Password </a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
		
		<div id="sub">
        <div class="ulLink">
            <ul>
              <li class="active">Manually Add</li>
              <li>|</li>
              <li><a href="/Super-Admin/adduser/sa-addUserCSV.php">Add by CSV File</a></li>       
            </ul>
          </div>
     </div>
		
		<div id="form">
			<div id="hi">
        <h3>Add User</h3>
        <p>Fill up this form to add user.</p>
      </div>
			
			<form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method ="POST" role="form" class="form-horizontal">
				<div class="form-group ">
					<label class="control-label">User ID <span style="color: #FF0000;">*</span> </label>
					<input type="text" name= "id" class="form-control" pattern="^(PES|HOD|ST)[0-9]{4}|^(B0)[1-3]{1}[0-9]{6}|^(F0)[1]{1}[0-9]{6}|^(D)([A-Z]){2}([0-9]){8}" min="1" max="200" style="width:500px;" maxlength="12" required/>
					<i><font size="2">Eg: SA0001 (Super Admin), PES0001 (Admin), DIT01150001 (Student)</font></i>
				</div>

				<div class="form-group ">
					<label class="control-label">Name <span style="color: #FF0000;">*</span> </label>
					<input class="form-control" type="text" pattern="([A-Za-z'.@-/ ])\w+" name="name" min="1" max="200" style="width:500px;" maxlength="100" required>
				</div>

				<div class="form-group ">
					<label class="control-label">NRIC <span style="color: #FF0000;">*</span> </label>
					<input class="form-control" type="text" pattern="([0-9]){12}" name= "nric" min="1" max="200" style="width:500px;" maxlength="12" required>
					<i><font size="2">Eg: 990102034556. Please insert without '-'</font></i>
				</div>

				<div class="form-group ">
					 <label class="control-label">Email <span style="color: #FF0000;">*</span> </label>
					<input class="form-control" type="email" name= "email" min="1" max="200" style="width:500px;" maxlength="30" required>
				</div>

				<div class="form-group ">
					<label class="control-label">Contact No. <span style="color: #FF0000;">*</span> </label>
					<input class="form-control" type="text" pattern="^(01)([0-9]){8,9}" name="contact" min="1" max="200" style="width:500px;" maxlength="11" required>
					<i><font size="2">Eg: 0199876543. Please insert without '-'</font></i>
				</div>

				<input class="btn btn-default" type="submit" value="Submit" name= "SUBMIT">
			
		</div>
		
				
	</body>
</html>


	<script src="/assets/js/jquery.min.js"></script>
      <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
      <script src="/assets/js/jquery.dataTables.min.js"></script>
      <script src="/assets/js/dataTables.bootstrap.min.js"></script>
      <script src="/assets/js/bootstrap-dialog.min.js"></script>
      <script type="text/javascript">
				
</script>
