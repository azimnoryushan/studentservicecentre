<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Super-Admin/adduser/sa-addUserBL.php');
?>

<html>
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add User - Super Admin</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link href="/assets/css/datepicker.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
  </head>
	
	<body>
		<div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li class="active" role="presentation"><a href="#">Add User </a></li>
							<li role="presentation"><a href="/Super-Admin/deleteuser/">Update | Delete User</a></li>
							<li role="presentation"><a href="/Super-Admin/auditlog/">Audit Log</a></li>
							<li role="presentation"><a href="/Super-Admin/changepassword/">Change Password </a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
		
		 <div id="sub">
        <div class="ulLink">
            <ul>
              <li><a href="/Super-Admin/adduser/">Manually Add</a></li>       
              <li>|</li>
              <li class="active">Add by CSV File</li>
            </ul>
          </div>
     </div>
				
		<div id="form">
			</form>
			<div id="hi">
        <h3>Add User</h3>
        <p>Upload *.csv file to add user.</p>
			</div>
			
			<form class="form-horizontal" action="<?php  echo $_SERVER['PHP_SELF']; ?>" method="POST" name="upload_excel" enctype="multipart/form-data">
				<label class="col-md-4 control-label" for="filebutton">Select File</label>
				<input class="col-md-4 control-label" type="file" name="file" id="file" class="input-large">

				<button type="submit" id="submit" name="IMPORT" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
			</form>
		</div>
			
				
				
	</body>
</html>


	<script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
  <script src="/assets/js/jquery.dataTables.min.js"></script>
  <script src="/assets/js/dataTables.bootstrap.min.js"></script>
  <script src="/assets/js/bootstrap-dialog.min.js"></script>
  <script type="text/javascript">
		$('#file').change(function(){	
			if($(this).val().search('.csv') <= 0){
				$(this).val('');
				BootstrapDialog.alert('Only support CSV.');
				return;
			}
		});
				
</script>
