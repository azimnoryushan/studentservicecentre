<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Super-Admin/auditlog/sa-auditBL.php');
?>

	<!DOCTYPE html>

	<html>

	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Audit Log Page</title>
		<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/Navigation-with-Button1.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/styles.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
</head>

	<body>
		<div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li role="presentation"><a href="/Super-Admin/adduser/">Add User</a></li>
							<li role="presentation"><a href="/Super-Admin/deleteuser/">Update | Delete User</a></li>
							<li class="active" role="presentation"><a href="#">Audit Log</a></li>
							<li role="presentation"><a href="/Super-Admin/changepassword/">Change Password </a></li>
						</ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
		
		<div id="form">
        <div id="hi">
          <h3>Audit Log</h3>
          <p>Super Admin can view all user activities.</p>
        </div>
    </div>
		
    <div id="parcelform">
      <table id="audit_log" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
					<tr>
						<th>No.</th>
						<th>Admin ID</th>
						<th>Time Log</th>
						<th>Activity Log</th>
					</tr>
				</thead>
				
				<tbody>
					<?php
					if(mysqli_num_rows($result) > 0){
						while($row=mysqli_fetch_array($result)){
					?>
					<tr>
						<td><?php echo $row['au_ID'];?></td>
						<td><?php echo $row['au_user_ID'];?></td>
						<td><?php echo $row['au_timeLog'];?></td>
						<td><?php echo $row['au_activity'];?></td>
					</tr>
					<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
  </body>
</html>


<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
<script src="/assets/js/jquery.dataTables.min.js"></script>
<script src="/assets/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-dialog.min.js"></script>
<script>
	$(document).ready(function() {
    		$('#audit_log').DataTable( {
        	"order": [[ 2, "desc" ]]
   			 } );
			} );
</script>

