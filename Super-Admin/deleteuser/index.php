<?php
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
$sa_user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/Super-Admin/deleteuser/superAdmin-SelectUserBL.php');
?>

<html>
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update | Delete User - Super Admin</title>
    <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
  </head>
	
	<body>
		<div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li role="presentation"><a href="/Super-Admin/adduser/">Add User </a></li>
							<li class="active" role="presentation"><a href="#">Update | Delete User</a></li>
							<li role="presentation"><a href="/Super-Admin/auditlog/">Audit Log</a></li>
							<li role="presentation"><a href="/Super-Admin/changepassword/">Change Password </a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label>
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
			
			
			
			<div id="form">
        <div id="hi">
          <h3>Update | Delete User</h3>
          <p>Head of each department will have the list of the forms submitted by respective student.&nbsp; </p>
        </div>
      </div>
			
			<div id="parcelform">
        <table id="userz" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>User ID</th>
							<th>Name</th>
							<th>NRIC</th>
							<th>Email</th>
							<th>Contact No</th>
							<th>Role</th>
							<th>Update | Delete</th>
						</tr>
					</thead>
					
					<tbody data-link="row" class="rowlink">
						<?php
              if(mysqli_num_rows($result) > 0){
                while($row=mysqli_fetch_array($result)){
                  ?>
                  <tr>
                    <td id="id<?php echo $row['us_ID'];?>"><?php echo $row['us_ID'];?></td>
										<td id="name<?php echo $row['us_ID'];?>"><?php echo $row['us_name'];?></td>
                    <td id="ic<?php echo $row['us_ID'];?>"><?php echo $row['us_NRIC'];?></td>
                    <td id="email<?php echo $row['us_ID'];?>"><?php echo $row['us_email'];?></td>
                    <td id="contact<?php echo $row['us_ID'];?>"><?php echo $row['us_contactNo'];?></td>
                    <td id="role<?php echo $row['us_ID'];?>"><?php echo $row['us_role'];?></td>
                    <td><button id="delete_record" onclick="openpopup_approve('<?php echo $row['us_ID'];?>')" class="btn btn-danger pull-right" style="width: 120px; margin-bottom: 10px; height: 40px;">Remove</button>
												<button class="btn  btn-primary btn-lg" style="width: 120px; margin-bottom: 10px; height: 40px;" onclick="getuser('<?php echo $row['us_ID'];?>', '<?php echo $row['us_name'];?>', '<?php echo $row['us_NRIC'];?>', '<?php echo $row['us_email'];?>', '<?php echo $row['us_contactNo'];?>')" data-toggle="modal" data-target="#submit-parcel"><span style="font-size:smaller;">Update User</span></button>

									</tr>
                  <?php
                }
              }
              ?>
					</tbody>
				</table>				
		</div>
		
		<div class="modal fade" id="submit-parcel" tabindex="-1" role="dialog" 
			aria-labelledby="submit-parcel">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							Update User 
						</h4>
					</div>
					<!-- Modal Body -->
					<div class="modal-body">
						<form id="update_form" class="form-horizontal" role="form">
							
							<div class="form-group">
								<label  class="col-sm-3 control-label" for="userID">User ID<span style="color: #FF0000;">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control"  id="userID" name="usID" maxlength="12" disabled/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="userName">Name<span style="color: #FF0000;">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="userName" name="usName" maxlength="100"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="userNRIC">NRIC<span style="color: #FF0000;">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="userNRIC" name="usNRIC" maxlength="12"/>
								</div>
							</div>                        
							<div class="form-group">
								<label class="col-sm-3 control-label" for="userEmail">Email<span style="color: #FF0000;">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="userEmail" name="usEmail" maxlength="30"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="userContact">Contact No<span style="color: #FF0000;">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="userContact" name="usContact" maxlength="13"/>
								</div>
							</div>
							<div class="form-group">
          <label class="col-sm-3 control-label" for="resetPass">Reset Password<span style="color: #FF0000;">*</span></label>
              
					<div class="col-sm-9">
                <select class="form-control" id="resetPass" required>
                  <option value="1">No</option>
                  <option value="2">Yes</option>
                </select>
              </div>
        </div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="buttondismiss" class="btn btn-default" data-dismiss="modal">Close</button>
						<input class="btn btn-primary" id="submit" type="submit" value = "Submit">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

	<script src="/assets/js/jquery.min.js"></script>
      <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
      <script src="/assets/js/jquery.dataTables.min.js"></script>
      <script src="/assets/js/dataTables.bootstrap.min.js"></script>
      <script src="/assets/js/bootstrap-dialog.min.js"></script>
      <script type="text/javascript">
				
			$(document).ready(function() {
    		$('#userz').DataTable( {
        	"order": [[ 1, "asc" ]],
					"columnDefs": [
    				{ "orderable": false, "targets": 6 }
  					]
   			 } );
			} );	
	function openpopup_approve(usID){
        BootstrapDialog.show({
          title: 'Confirm Approval',
          message: 'Are you confirm?',
          buttons: [{
            label: 'Yes',
            action: function(dialog) {
              removeUSER(usID);
              dialog.close();
            }
          }, {
            label: 'No',
            action: function(dialog) {
              dialog.close();
            }
          }]
        });
      }			

	function removeUSER(usID){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'success'){
              BootstrapDialog.alert('Successfully delete user.Please refresh.');
            }
						else{
							BootstrapDialog.alert('Failed to remove.Please refresh.');
						}
          }
        };
        xhttp.open("GET", "superAdmin-DeleteUserBL.php?id=" + usID, true);
        xhttp.send();
      }

  //button "Submit"
$('#submit').on('click', function(){
  submit_form();
});
  
  //button "Dismiss"
$('#buttondismiss').click(function(){
  var update_form = document.getElementById('update_form');
  update_form.reset();
})

  //submit form
function submit_form(){
  
  var update_form = document.getElementById('update_form');
  var userID = document.getElementById('userID').value,
  userName = document.getElementById('userName').value,
  userNRIC = document.getElementById('userNRIC').value,
  userEmail = document.getElementById('userEmail').value,
	userpass = $('#resetPass').val(),
  userContact = document.getElementById('userContact').value;
  if(userID.length == 0 || userName.length == 0 || userNRIC.length == 0 || userEmail.length == 0 || userContact.length == 0){
		BootstrapDialog.alert('Please fill all the information');
		return;
	}
  updateuser(userID, userName, userNRIC, userEmail, userContact, userpass);
  update_form.reset();
  
  return false;
}
				
function getuser(userID, userName, userNRIC, userEmail, userContact){
	$('#userID').val(userID);
	$('#userName').val(userName);
	$('#userNRIC').val(userNRIC);
	$('#userEmail').val(userEmail);
	$('#userContact').val(userContact);
}
  
function updateuser(userID, userName, userNRIC, userEmail, userContact, userpass){
	var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'fail'){
             	BootstrapDialog.alert('Fail to update user information.');
            }
						else{
							json = JSON.parse(this.responseText);
							
							$('#buttondismiss').click();
						 	BootstrapDialog.alert('Successfully update user information. Please refresh.');
						}
          }
        };
        xhttp.open("GET", "superAdmin-UpdateUserBL.php?userID="
				+userID+
				"&userName="
				+userName+
				"&userNRIC="
				+userNRIC+
				"&userEmail="
				+userEmail+
				"&userContact="
				+userContact+
				"&resetpassword=" + userpass, true);
        xhttp.send();

}

</script>