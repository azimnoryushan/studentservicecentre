<?php
if(isset($_GET['userName']) && isset($_GET['userID']) && isset($_GET['userNRIC']) && isset($_GET['userEmail']) && isset($_GET['userContact']) && isset($_GET['resetpassword'])){
  session_start();
  include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
	$userID = $_SESSION['myuserID'];
  $row_name = strtoupper(mysqli_real_escape_string($conn, $_GET['userName']));
  $row_id = mysqli_real_escape_string($conn, $_GET['userID']);
  $row_nric = mysqli_real_escape_string($conn, $_GET['userNRIC']);
  $row_email = mysqli_real_escape_string($conn, $_GET['userEmail']);
  $row_contact = mysqli_real_escape_string($conn, $_GET['userContact']);
  $row_pass = $_GET['resetpassword'];
	$row_role = substr($row_id, 0, 1);
	
	
	if ($row_role == "B" || $row_role == "F" || $row_role == "D"){
    $row_role = 'STUD';
		$row_pass = md5($row_nric); //default password STUD = IC no.
  }else if ($row_role == "P"){
    $row_role = 'ADMIN';
		$row_pass = md5('1234'); //default password ADMIN = 1234
  }else if ($row_role == "H"){
		$row_role = 'HOD';
		$row_pass = md5('1234'); //default password ADMIN = 1234
	}else if ($row_role == "R"){
		$row_role = 'SUBSCRIBER';
		$row_pass = md5('1234'); //default password ADMIN = 1234
	}else{
		?>
		<script> alert("Invalid role");</script>
		<?php
	}
  
	if($row_pass == 1){
		//audit activity
  	$au_activity ="UPDATED user ID:$row_id";
		
		mysqli_query($conn,"SET FOREIGN_KEY_CHECKS = 0");
		$sql = "UPDATE user SET us_ID='$row_id' ,us_name='$row_name',us_email='$row_email',us_contactNo='$row_contact',us_role='$row_role',us_NRIC='$row_nric' WHERE us_ID='$row_id'";
		$au_sql = "INSERT INTO audit (au_user_ID, au_timeLog, au_activity) VALUES ('$userID', now(), '$au_activity')";
				
		if(mysqli_query($conn,$sql) && mysqli_query($conn,$au_sql)){
			echo json_encode(array($row_name, $row_id, $row_nric, $row_email, $row_contact, $row_role));
		}
		else{
			echo "fail";
		}
		mysqli_query($conn,"SET FOREIGN_KEY_CHECKS = 1");
	}
	else{
		 //audit activity
  	$au_activity ="UPDATED and RESET PASSWORD user ID:$row_id";
		
		mysqli_query($conn,"SET FOREIGN_KEY_CHECKS = 0");
  	$sql = "UPDATE user SET us_ID='$row_id' ,us_name='$row_name',us_password='$row_pass',us_email='$row_email',us_contactNo='$row_contact',us_role='$row_role',us_NRIC='$row_nric' WHERE us_ID='$row_id'";
	$au_sql = "INSERT INTO audit (au_user_ID, au_timeLog, au_activity) VALUES ('$userID', now(), '$au_activity')";
		
  
		if(mysqli_query($conn,$sql) && mysqli_query($conn,$au_sql)){
    echo json_encode(array($row_name, $row_id, $row_nric, $row_email, $row_contact, $row_role));
  }
  else{
    echo "fail";
  }
  mysqli_query($conn,"SET FOREIGN_KEY_CHECKS = 1");
	}
  

  
  
}
?>
