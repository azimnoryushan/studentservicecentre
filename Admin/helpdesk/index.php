<!DOCTYPE html>
<html>
<?php 
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
$pes_user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Admin/helpdesk/A-helpdeskBL.php');
?>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Help Desk Page</title>
  
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
</head>

<body>
  <div>
    <nav class="navbar navbar-default navbar-fixed-top navigation-clean-button">
      <div class="container">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE</a>
          <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
          <ul class="nav navbar-nav">
            <li role="presentation"><a href="/Admin/parcel/">Parcel Submission</a></li>
            <li role="presentation"><a href="/Admin/transportation/">Transportation Booking</a></li>
            <li class="active" role="presentation"><a href="#">Help Desk</a></li>
            <li role="presentation"><a href="/Admin/changepassword/">Change Password </a></li>
          </ul>
          <p class="navbar-text navbar-right actions">
            <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Log out</a></p>
            </div>
          </div>
        </nav>
      </div>
      
      <div id="form">
        <div id="hi">
          <h3>Help Desk</h3>
          <p>Admin can entertain students' inquires &nbsp; </p>
        </div>
      </div>
      <div id="parcelform">
        <table id="parcel_data" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Description</th>
              <th>Type</th>
              <th>Student ID</th>
              <th>Date & Time</th>
              <th>Status</th>
							<th>Remarks</th>
              <th>Change Status</th>
            </tr>
          </thead>
            <tbody>
              <?php
              if(mysqli_num_rows($result) > 0){
                while($row=mysqli_fetch_array($result)){
                  ?>
                  <tr>
                    <td><?php echo $row['hd_ID'];?></td>
                    <td><?php echo $row['hd_title'];?></td>
                    <td><?php echo $row['hd_description'];?></td>
                    <td><?php echo $row['ct_name'];?></td>
                    <td><?php echo $row['hd_us_ID_fk'];?></td>
                    <td><?php echo $row['hd_date'];?></td>
                    <td id="th<?php echo $row['hd_ID'];?>"><?php echo $row['hd_status'];?></td>
                    <td><?php echo $row['hd_remarks'];?></td>
                    <th>
												<input <?php if($row['hd_status'] == 'Accepted') echo "disabled"; ?> id="button<?php echo $row['hd_ID'];?>" class="btn btn-default" type="submit" value="Accepted" onclick="openpopup_approve(<?php echo $row['hd_ID'];?>), sentemail(<?php echo $row['hd_ID'];?>, '<?php echo $row['hd_title'];?>', '<?php echo $row['hd_date'];?>')"></br>
                      </form>
                    </th>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
            
          </tbody>
        </table>
      </div>
    </body>
    </html>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.bootstrap.min.js"></script>
		<script src="/assets/js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript">
			var textremark;
			function openpopup_approve(val){
				BootstrapDialog.show({
            title: 'Comment',
            message: $('<input type="text" id="textremark" class="form-control" placeholder="Comment">'),
            buttons: [{
                label: 'Enter',
                cssClass: 'btn-primary',
                action: function(dialog) {
                  textremark = $('#textremark').val();
                  dialog.close();
									
        BootstrapDialog.show({
          title: 'Confirm Acceptence',
          message: 'Are you confirm?',
          buttons: [{
            label: 'Yes',
            action: function(dialog) {
              changestatus(val, 1, textremark);
              dialog.close();
            }
          }, {
            label: 'No',
            action: function(dialog) {
              dialog.close();
            }
                  }]
                });
                }
            }]
        });
      }
										
    $(document).ready(function() {
      $('#parcel_data').DataTable({
				"columnDefs": [
    	{ "orderable": false, "targets": 7 }
				
  ]
			});
			
    } );
			
     function changestatus(val, type, str){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'success'){
							if(type == 1) var string = 'Accepted';
							$('#th' + val).html(string);
              $('#button' + val).attr("disabled", true);
              BootstrapDialog.alert('Successfully status changed');
            }
						else{
							BootstrapDialog.alert('Failed to update');
						}
          }
        };
        xhttp.open("GET", "A-helpdeskBL.php?id=" + val + "&type=" + type + "&str_remarks=" + str, true);
        xhttp.send();
      }
			function sentemail(id, title, date){
				var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
          }
        };
        xhttp.open("GET", "/assets/mail/autoemail-helpdesk.php?id=" + id + "&title=" + title + "&date=" + date + "&token=ajimbusuk", true);
        xhttp.send();
			}
    
    </script>