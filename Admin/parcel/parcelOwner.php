<?php
    include($_SERVER['DOCUMENT_ROOT'].'/shared/Database.php');

    if(isset($_POST["query"]))
    {
        $output = '';
        $query = "SELECT us_name, us_email FROM user WHERE us_name LIKE '%".$_POST["query"]."%'";
        $result = mysqli_query($conn, $query);
        $output = '<ul class="list-unstyled" style="cursor:pointer;">';
        if (mysqli_num_rows($result)>0)
        {
            while($row = mysqli_fetch_array($result))
            {
                $output .= '<li style="padding: 12px;">'.$row["us_name"]. ' ' .$row["us_email"] .'</li>';
            }
        }
        else
        {
            $output .= '<option style="padding: 12px; cursor: default;" disabled>Parcel owner not found</option>';
        }
        $output .='</ul>';
        echo $output;
    }
?>          


<!-- AUDIT CODE
$user = $_SESSION['myuserID'];
$activity = "CHANGE STATUS Parcel Submission FROM Pending TO Reciceved";
$sql_audit = "INSERT INTO audit (au_user_ID_fk, au_dateLog, au_timeLog, au_activityLog) VALUES ('$user', date("Y-m-d"), date("h:i:s"), '$activity')";
    -->