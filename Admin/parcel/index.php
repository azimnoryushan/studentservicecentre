<!DOCTYPE html>
<html>
<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] .'/shared/Database.php');
$pes_user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include($_SERVER['DOCUMENT_ROOT'] .'/Admin/parcel/A-parcelBL.php');
	
?>


<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Parcel-Admin</title>
  <link rel="stylesheet"  type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/Navigation-with-Button1.css">
  <link rel="stylesheet" type="text/css" href="/assets/css/styles.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
</head>

<body>
  <div>
    <nav class="navbar navbar-default navbar-fixed-top navigation-clean-button">
      <div class="container">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE</a>
          <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div> 
        <div class="collapse navbar-collapse" id="navcol-1">
          <ul class="nav navbar-nav">
            <li class="active" role="presentation"><a href="#">Parcel Submission</a></li>
            <li role="presentation"><a href="/Admin/transportation/">Transportation Booking</a></li>
            <li role="presentation"><a href="/Admin/helpdesk/">Help Desk</a></li>
            <li role="presentation"><a href="/Admin/changepassword/">Change Password </a></li>
          </ul>
          <p class="navbar-text navbar-right actions">
						<label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label>
            <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Log out</a></p>
          </div>
        </div>
      </nav>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="submit-parcel" tabindex="-1" role="dialog" 
    aria-labelledby="submit-parcel">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
            Submit Parcel 
          </h4>
        </div>
        <!-- Modal Body -->
        <div class="modal-body">
          <form id="parcel_form" class="form-horizontal" role="form">
            <div class="form-group">
              <label  class="col-sm-3 control-label" for="trackingNo">Tracking No<span style="color: #FF0000;">*</span></label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  id="trackingNo" placeholder="Parcel Tracking Number" name="trackNo" required/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="parcelOwner">Name<span style="color: #FF0000;">*</span></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="parcelOwner" placeholder="Parcel Owner" name="parcelOwner" required/>
                <div id="userlist"></div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="statusID">Status<span style="color: #FF0000;">*</span></label>
              <div class="col-sm-9">
                <select class="form-control" id="statusID" required>
                  <option value="1">Pending</option>
                  <option value="2">Claimed</option>
                </select>
              </div>
            </div>                        
            <div class="form-group">
              <label class="col-sm-3 control-label" for="remark" >Remark</label>
              <div class="col-sm-9">
                <textarea type="text" class="form-control" id="remark" placeholder="Notes to be added"></textarea>
              </div>
            </div>
          </form>
        </div>
        <!-- Modal Footer -->
        <div class="modal-footer">
          <button type="button" id="buttondismiss" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" id="submit" type="submit" value = "Submit">
        </div>
      </div>
    </div>
  </div>
  
  
  <div id="form">
    
    <div id="hi">
      <h3>Parcel Submission</h3>
      <p>Admin may click the button to submit the received parcel below&nbsp; </p>
    </div>
    <div class="wrapper">
      <button class="btn btn-default btn-lg" data-toggle="modal" id="addparcel" data-target="#submit-parcel"><span style="font-size:smaller;">Add Parcel</span></button>
    </div>
  </div>
  <div id="parcelform">
    <div class="container">
      <p>This table show the record of parcels received by CSE@UniMy.</p>
    </div>
    <table id="parcel_data" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Tracking Number</th>
          <th>Parcel Owner</th>
          <th>Date & Time</th>
          <th>Contact No.</th>
          <th>Remark</th>
          <th>Status</th>
					<th>Change Status</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if(mysqli_num_rows($result) > 0){
          while($row=mysqli_fetch_array($result)){
            ?>
            <tr>
              <td><?php echo $row['pc_trackID'];?></td>
              <td><?php echo $row['2'];?></td>
              <td><?php echo $row['timeReceived'];?></td>
              <td><?php echo $row['4'];?></td>
              <td><?php echo $row['pc_note'];?></td>
              <td id="haha<?php echo $row['pc_ID'];?>"><?php echo $row['pc_status'];?></td>
              <td>
                <input <?php if($row['pc_status'] == 'Claimed') 
									echo "disabled";?> id="buttonapprove<?php echo $row['pc_ID'];?>" 
											 	style="width: 90px; margin-bottom: 5px;" class="btn btn-default" type="submit" value="Claimed" onClick="openpopup_claimed('<?php echo $row['pc_ID'];?>', '<?php echo $row['pc_trackID'];?>' )">
              </td>
             </tr>
              <?php
            }
          }
          ?>
      </tbody>
    </table>
  </div>
</body>
</html>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
<script src="/assets/js/jquery.dataTables.min.js"></script>
<script src="/assets/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript">
	
	
  function openpopup_claimed(val, track){
        BootstrapDialog.show({
          title: 'Confirm Acceptence',
          message: 'Are you confirm?',
          buttons: [{
            label: 'Yes',
            action: function(dialog) {
              changestatus(val, track, 2);
              dialog.close();
            }
          }, {
            label: 'No',
            action: function(dialog) {
              dialog.close();
            }
          }]
        });
      }
	
	function changestatus(val, track, type){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'success'){
							if(type == 2) var string = 'Claimed';
							$('#haha').val(string);
              $('#buttonapprove' + val).attr("disabled", true);
              BootstrapDialog.alert('Successfully status changed');
            }
						else{
							BootstrapDialog.alert('Failed to update');
						}
          }
        };
        xhttp.open("GET", "A-parcelBL.php?id=" + val + "&type=" + type + "&trackingNo=" + track, true);
        xhttp.send();
      }

$(document).ready(function(){  
  
  $('#parcel_data').DataTable( {
		"columnDefs": [
    { "orderable": false, "targets": 6 }
  ],
		stateSave: true
  });  
  
  $('#parcelOwner').keyup(function(e) {
    var query = $(this).val();
    
    if (query.length > 0){
      $.ajax({
        url:"parcelOwner.php",
        method:"POST",
        data:{query:query},
        
        success:function(data)
        {
          $('#userlist').fadeIn();
          $('#userlist').html(data);
        }
      });
    }
    else $('#userlist').html('');
  });
  
  $(document).on('click','li', function() {
    $('#parcelOwner').val($(this).text());
    $('#userlist').fadeOut();
  });
});
	
function submit_form(){
  
  var parcel_form = document.getElementById('parcel_form');
  var trackingNo = document.getElementById('trackingNo').value,
  parcelOwner = document.getElementById('parcelOwner').value,
  parcelStatus = document.getElementById('statusID').value,
  remark = document.getElementById('remark').value;
  
  parcelOwner = parcelOwner.split(" ");
  var email = parcelOwner.slice(-1);
  parcelOwner = parcelOwner.slice(0, -1).join(' ');
  saveparcel(trackingNo, email, parcelStatus, remark);
  sentemail(trackingNo, email, parcelOwner);
  parcel_form.reset();
  
  return false;
}

function saveparcel(trackingNo, parcelOwner, parcelStatus, remark){
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if(this.responseText == 'success'){
        $('#buttondismiss').click();
        BootstrapDialog.alert('Parcel information has been recorded.');
      }
      else{
        BootstrapDialog.alert('Parcel information failed to save.');
      }
    }
  };
  
  xhttp.open("GET", 'A-parcelBL.php?trackingNo='
  +trackingNo+
  '&parcelOwner='
  +parcelOwner+
  '&statusID='
  +parcelStatus+
  '&remark='
  +remark, true);
  xhttp.send();
}

function sentemail(parcelid, email, name){
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    }
  };
  
  xhttp.open("GET", '/assets/mail/autoemail-parcel.php?email=' + email + '&name=' + name + '&parcel=' + parcelid + '&token=ajimbusuk', true);
  xhttp.send();
}

$('#submit').on('click', function(){
  submit_form();
});

$('#buttondismiss').click(function(){
  var parcel_form = document.getElementById('parcel_form');
  parcel_form.reset();
})
</script>















