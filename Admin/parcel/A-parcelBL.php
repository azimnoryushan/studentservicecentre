<?php


if (isset($_GET['trackingNo']) && isset($_GET['parcelOwner']) && isset($_GET['statusID']) && isset($_GET['remark'])){
  
  session_start();
  include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
  $userID = $_SESSION['myuserID'];
  
  $trackingNo = mysqli_real_escape_string($conn, strip_tags($_GET['trackingNo']));
  $parcelOwner = mysqli_real_escape_string($conn, strip_tags($_GET['parcelOwner']));
  date_default_timezone_set("Asia/Kuala_Lumpur");
  $timeArrived = mysqli_real_escape_string($conn, strip_tags(date('Y-m-d H:i:s')));
  $parcelStatus = mysqli_real_escape_string($conn, strip_tags($_GET['statusID']));
  $remark = mysqli_real_escape_string($conn, strip_tags($_GET['remark']));
  $check = "SELECT * FROM user WHERE us_email = '$parcelOwner'";
  
  if($result=mysqli_query($conn,$check)){
    $rowcount=mysqli_num_rows($result);
    if($rowcount == 0){
      echo "fail"; return;
    }
  }
  $ins_sql = "INSERT INTO parcel (pc_ID, pc_trackID, timeReceived, pc_note, pc_status, pc_user_ID_fk, pc_contactNo) 
  SELECT  '',  '$trackingNo',  '$timeArrived',  '$remark',  '$parcelStatus', us_ID, us_contactNo
  FROM user WHERE us_email =  '$parcelOwner'";
  //audit activity
  $au_activity = "INSERTED TrackingNO:$trackingNo into Parcel Database";
  
  $au_sql = "INSERT INTO audit (au_user_ID, au_timeLog, au_activity) VALUES ('$userID', now(), '$au_activity')";
  
  
  if (mysqli_query($conn, $ins_sql) && mysqli_query($conn,$au_sql)) {
      echo "success"; return;
  }
  else{
    echo "fail"; return;
  }
  
}
else if (isset($_GET['type']) && isset($_GET['id']) && isset($_GET['trackingNo'])){
  
  session_start();
  include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
  $userID = $_SESSION['myuserID'];
  
  $trackingNo = mysqli_real_escape_string($conn, strip_tags($_GET['trackingNo']));
  $row_id = mysqli_real_escape_string($conn, $_GET['id']);
  $row_type = mysqli_real_escape_string($conn, $_GET['type']);
  
  
  if ($row_type == 2) {
    //audit activity
    $au_activity = "CHANGED TrackingNO:$trackingNo Parcel Status FROM Pending TO Claimed";
    $sql = "UPDATE parcel SET pc_status = '$row_type' WHERE pc_ID = '$row_id'";
     $au_sql = "INSERT INTO audit (au_user_ID, au_timeLog, au_activity) VALUES ('$userID', now(), '$au_activity')";
    
  if(mysqli_query($conn,$sql) && mysqli_query($conn,$au_sql)){
    echo "success"; return;
  }
  else{
    echo "fail"; return;
  }
    
  }
}
else{
   
  $sql = "SELECT DISTINCT parcel.pc_ID, parcel.pc_trackID, user.us_name, parcel.timeReceived, user.us_contactNo, parcel.pc_note, parcel.pc_status FROM parcel INNER JOIN user ON parcel.pc_user_ID_fk = user.us_ID ORDER BY pc_ID DESC;";  
  
  $result = mysqli_query($conn, $sql); 
  }
 ?>