<!DOCTYPE html>
<html>
<?php 
session_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
$user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Admin/transportation/A-transportationBL.php');
?>  

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Transportation Page</title>
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
  <link rel="stylesheet" href="/assets/css/bootstrap-dialog.min.css">
</head>

<body>
  <div>
    <nav class="navbar navbar-default navbar-fixed-top navigation-clean-button">
      <div class="container">
        <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE</a>
          <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div> 
        <div class="collapse navbar-collapse" id="navcol-1">
          <ul class="nav navbar-nav">
            <li role="presentation"><a href="/Admin/parcel/">Parcel Submission</a></li>
            <li class="active" role="presentation"><a href="#">Transportation Booking</a></li>
            <li role="presentation"><a href="/Admin/helpdesk/">Help Desk</a></li>
            <li role="presentation"><a href="/Admin/changepassword/">Change Password </a></li>
          </ul>
          <p class="navbar-text navbar-right actions">
            <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Log out</a></p>
            </div>
          </div>
        </nav>
      </div>
      
      <div id="form">
        <div id="hi">
          <h3>Transportation</h3>
          <p>Admin can approve or reject the transportation request &nbsp; </p>
        </div>
      </div>
      <div id="parcelform">
        <table id="parcel_data" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>From (Date&Time)</th>
              <th>To (Date&Time)</th>
              <th>Purpose</th>
              <th>Club Name</th>
              <th>Destination</th>
              <th>Transport Type</th>
              <th>Status</th>
              <th>Change Status</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(mysqli_num_rows($result) > 0){
              while($row=mysqli_fetch_array($result)){
                ?>
                <tr>
                  <td><?php echo $row['tb_ID'];?></td>
                  <td><?php echo $row['tb_datetimeFrom'];?></td>
                  <td><?php echo $row['tb_datetimeTo'];?></td>
                  <td><?php echo $row['tb_purpose'];?></td>
                  <td><?php echo $row['tb_clubName'];?></td>
                  <td><?php echo $row['tb_destination'];?></td>
                  <td><?php echo $row['tb_type'];?></td>
                  <td id="th<?php echo $row['tb_ID'];?>"><?php echo $row['tb_status'];?></td>
                  <td>
                    <input <?php if($row['tb_status'] == 'Approved' || $row['tb_status'] == 'Rejected') echo "disabled"; ?> id="buttonapprove<?php echo $row['tb_ID'];?>" style="width: 90px; margin-bottom: 5px;" class="btn btn-default" type="submit" value="Approve" onclick="openpopup_approve(<?php echo $row['tb_ID'];?>), sentemail(<?php echo $row['tb_ID'];?>, '<?php echo $row['tb_datetimeFrom'];?>', '<?php echo $row['tb_datetimeTo'];?>', '<?php echo $row['tb_purpose'];?>', '<?php echo $row['tb_clubName'];?>', '<?php echo $row['tb_destination'];?>', '<?php echo $row['tb_type'];?>')"></br>
                      <input <?php if($row['tb_status'] == 'Approved' || $row['tb_status'] == 'Rejected') echo "disabled"; ?> id="buttonreject<?php echo $row['tb_ID'];?>" style="width: 90px;" class="btn btn-default" type="submit" value="Reject" onclick="openpopup_reject(<?php echo $row['tb_ID'];?>), sentemailreject(<?php echo $row['tb_ID'];?>, '<?php echo $row['tb_datetimeFrom'];?>', '<?php echo $row['tb_datetimeTo'];?>', '<?php echo $row['tb_purpose'];?>', '<?php echo $row['tb_clubName'];?>', '<?php echo $row['tb_destination'];?>', '<?php echo $row['tb_type'];?>')"></br>
                      </form>
                    </td>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </body>
      </html>
      <script src="/assets/js/jquery.min.js"></script>
      <script src="/assets/bootstrap/js/bootstrap.min.js"></script>  
      <script src="/assets/js/jquery.dataTables.min.js"></script>
      <script src="/assets/js/dataTables.bootstrap.min.js"></script>
      <script src="/assets/js/bootstrap-dialog.min.js"></script>
      <script type="text/javascript">
				
      function openpopup_approve(val){
        BootstrapDialog.show({
          title: 'Confirm Approval',
          message: 'Are you confirm?',
          buttons: [{
            label: 'Yes',
            action: function(dialog) {
              changestatus(val, 1, '');
              dialog.close();
            }
          }, {
            label: 'No',
            action: function(dialog) {
              dialog.close();
            }
          }]
        });
      }
       	
      var textremark;
      function openpopup_reject(val){
			BootstrapDialog.show({
            title: 'Rejection Remarks',
            message: $('<input type="text" id="textremark" class="form-control" placeholder="Why do you reject the request?">'),
            buttons: [{
                label: 'Enter',
                cssClass: 'btn-primary',
                action: function(dialog) {
                  textremark = $('#textremark').val();
                  dialog.close();
                  
                  BootstrapDialog.show({
                  title: 'Confirm Rejection',
                  message: 'Are you confirm?',
                  buttons: [{
                    label: 'Yes',
                    action: function(dialog) {
                      changestatus(val, 2, textremark);
                      dialog.close();
                    }
                  }, {
                    label: 'No',
                    action: function(dialog) {
                      dialog.close();
                    }
                  }]
                });
                }
            }]
        });
      }
				
      $(document).ready(function() {
        $('#parcel_data').DataTable({"columnDefs": [
    { "orderable": false, "targets": 8 }
  ]});
      } );
				
      function changestatus(val, type, str){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'success'){
							if(type == 1) var string = 'Accepted';
							if(type == 2) var string = 'Rejected';
							$('#th' + val).html(string);
              $('#buttonapprove' + val).attr("disabled", true);
							$('#buttonreject' + val).attr("disabled", true);
              BootstrapDialog.alert('Successfully status changed');
            }
						else{
							BootstrapDialog.alert('Failed to update');
						}
          }
        };
        xhttp.open("GET", "A-transportationBL.php?id=" + val + "&type=" + type + "&str_remarks=" + str, true);
        xhttp.send();
      }
				
			function sentemail(id, from, to, purpose, club, destination, type){
				var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
          }
        };
        xhttp.open("GET", "/assets/mail/autoemail-transportation.php?id=" + id + "&from=" + from + "&to=" + to + "&purpose=" + purpose + "&club=" + club + "&destination=" + destination + "&type=" + type + "&token=ajimbusuk", true);
        xhttp.send();
			}
				
			function sentemailreject(id, from, to, purpose, club, destination, type){
				var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
          }
        };
        xhttp.open("GET", "/assets/mail/autoemail-transportationReject.php?id=" + id + "&from=" + from + "&to=" + to + "&purpose=" + purpose + "&club=" + club + "&destination=" + destination + "&type=" + type + "&token=ajimbusuk", true);
        xhttp.send();
			}
      </script>