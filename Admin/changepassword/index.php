<?php
session_start();

include ($_SERVER['DOCUMENT_ROOT'] . '/shared/Database.php');
$hd_user = $_SESSION['myuserID'];
include ($_SERVER['DOCUMENT_ROOT'] . '/shared/checksession.php');
include ($_SERVER['DOCUMENT_ROOT'] . '/Admin/changepassword/A-changepasswordBL.php');
?>

<!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Change Password Page</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/Navigation-with-Button1.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link href="/assets/css/datepicker.min.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div>
      <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
          <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">CSE </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav">
              <li role="presentation"><a href="/Admin/parcel/">Parcel Submission</a></li>
              <li role="presentation"><a href="/Admin/transportation/">Transportation Booking</a></li>
              <li role="presentation"><a href="/Admin/helpdesk/">Help Desk</a></li>
              <li class="active" role="presentation"><a href="#">Change Password </a></li>
            </ul>
            <p class="navbar-text navbar-right actions">
              <label for="ID" style="padding-right: 20px;"><?php echo $_SESSION['myuserID']; ?></label> 
              <a class="btn btn-default action-button" role="button" href="/shared/logout.php">Logout </a></p>
          </div>
        </div>
      </nav>
    </div>
    <div id="form">
      <div id="hi">
        <h3>Change Password</h3>
        <p>Fill up this form to change the password.</p>
      </div>

      <form autocomplete="off" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" role="form" class="form-horizontal">
        <div class="form-group">
          <label class="control-label">Current Password </label>
          <input type="password" class="form-control" name="currentpassword" placeholder="Enter your current password" maxlength="12" required/></div>

        <div class="form-group">
          <label class="control-label">New Password</label>
          <input class="form-control" type="password" placeholder="Enter your new password" name="newpassword" maxlength="12" required></div>

        <div class="form-group">
          <label class="control-label">Confirm New Password </label>
          <input class="form-control" type="password" placeholder="Re-enter your new password" name="c-newpassword" maxlength="12" required></div>

        <input class="btn btn-default" type="submit" value="Submit" name="submit">
      </form>

    </div>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-animation.js"></script>
    <script src="/assets/js/script.js"></script>
  </body>

  </html>