<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  
  $admin_session = $_SESSION['myuserID'];
  
  if(isset($_POST['submit'])){
    
    $CURRENT_PASSWORD = $_POST['currentpassword'];
    $NEW_PASSWORD = $_POST['newpassword'];
    $CONFIRM_PASSWORD = $_POST['c-newpassword'];
    
    $CURRENT_PASSWORD = md5($CURRENT_PASSWORD);
    $NEW_PASSWORD = md5($NEW_PASSWORD);
    $CONFIRM_PASSWORD = md5($CONFIRM_PASSWORD);
    
    $sql = "SELECT us_password FROM user WHERE us_ID = '$admin_session'";
    $result = mysqli_query($conn,$sql);
    $userinfo = mysqli_fetch_array($result);
    $OLD_PASSWORD_DB = $userinfo['us_password'];
    
    if($OLD_PASSWORD_DB != $CURRENT_PASSWORD){
      ?><script>
        alert("Sorry password is incorrect");
      </script> <?php
    }
    else{
      if($NEW_PASSWORD != $CONFIRM_PASSWORD){
        ?><script>
          alert("Sorry password does not match");
      </script><?php
      }
      else{
        //audit activity
        $au_activity = "CHANGED PASSWORD";
        $sql = "UPDATE user SET us_password = '$CONFIRM_PASSWORD' WHERE us_ID = '$admin_session'";
        $au_sql = "INSERT INTO audit (au_user_ID, au_timeLog, au_activity) VALUES ('$admin_session', now(), '$au_activity')";
        mysqli_query($conn,$sql);
        mysqli_query($conn,$au_sql);
                
        ?><script>
         alert("Password changed");
        </script><?php
      }
    }
  }
}
?>

<!-- AUDIT CODE
$user = $_SESSION['myuserID'];
$activity = "UPDATE Password";
$sql_audit = "INSERT INTO audit (au_user_ID_fk, au_dateLog, au_timeLog, au_activityLog) VALUES ('$user', date("Y-m-d"), date("h:i:sa"), '$activity')";
mysqli_query($conn,$sql_audit);
    -->